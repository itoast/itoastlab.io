+++
date = "2017-04-15T19:11:42-05:00"
title = "members"
+++

* __Insook Wessels__ 
* __Ezequiel Guzman__ (_Vice President of Education_)
* __Cecilia Cabrera-Hernandez__ (_President_)
* __Kayoko Kimura__ 
* __Maria Isabel Lyon-Fiestas__ 
* __Mahesh Bhandari__ (_Sergeant-At-Arms_)
* __Roman Lynch__
* __Phil Barutha__
* __Doris Santiago__ (_Treasurer_)
* __Paul Valencia__ (_Vice President of Membership_)
* __Ahmad Al-Kofahi__
* __Ivan Castro__
* __Ilker Caraca__
* __Susmita Bhandari__ (_Secretary_)
* __Dhenusha Shrestha__
* __Gaurav Rawal__
* __Sofia Ingersoll__
* __Ahmad Ahmad__
* __Thang Tran__
* __Soham Pal__ (_Vice President of Public Relations_)
